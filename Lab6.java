import java.util.Scanner;

class Bank{
	private int balance;

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}
}
class Deposit extends Thread{
	private Bank bank;

	public Deposit(Bank bank) {
		super();
		this.bank = bank;
	}
	
	@Override
	public void run() {
		System.out.println("in Deposit, current balance = " + this.bank.getBalance());
		synchronized (bank) {
			int bal = this.bank.getBalance();
			bal++;
			if (bal>0) {
				bank.notifyAll();
				System.out.println("Notifying ...");
			}
			try {
				Thread.sleep((long)(Math.random()*10));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.bank.setBalance(bal);
		}
	
		
	}
}
class Widraw extends Thread{
	private Bank bank;

	public Widraw(Bank bank) {
			this.bank = bank;
	}
	
	@Override
	public void run() {
		System.out.println("in Widraw, current balance = " + this.bank.getBalance());
		try {
			
		synchronized (bank) {
			int bal = this.bank.getBalance();
			if (bal>0) {
				System.out.println("Balance is > 0 ");
				}
			else {
				System.out.println("Balance is 0 or less than zero, invoking bank.wait");
				bank.wait();
			}
			bal--;
			
			Thread.sleep((long)(Math.random()*10));
		
		this.bank.setBalance(bal);
		}
		} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		}
	
	
}
public class Lab6 {
public static void main(String[] args) throws InterruptedException {
	
	Scanner scanner = new Scanner(System.in);
	System.out.println("Enter a number to continue");
	scanner.nextInt();
	
	
	
	Bank bank  = new Bank();
	for (int i = 0;i < 10;i++) {
		Deposit d = new Deposit(bank);
		Widraw w = new Widraw(bank);
		d.start();
		w.start();
	}
	
	System.out.println("Current Balance = " + bank.getBalance());
	Thread.sleep(1000);
	System.out.println("Current Balance = " + bank.getBalance());
	Thread.sleep(1000);
	System.out.println("Current Balance = " + bank.getBalance());

}
}
